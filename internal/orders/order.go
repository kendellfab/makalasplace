package orders

import (
	"fmt"
	"hash/fnv"
	"strconv"
	"time"

	"gitlab.com/kendellfab/makalasplace/internal/items"
	"gitlab.com/kendellfab/makalasplace/internal/municipality"
)

// Order represents an order from the system
type Order struct {
	ID           uint64
	Municipality municipality.Municipality
	Cocoas       []items.Cocoa
	When         time.Time
}

// FromRecord parses a csv record item
func FromRecord(record []string) Order {
	o := Order{}
	o.ID, _ = strconv.ParseUint(record[0], 10, 64)
	o.Municipality = municipality.Municipality{Name: record[1], County: record[2]}
	o.Cocoas = items.DecodeArray(record[3])
	o.When, _ = time.Parse(time.RFC3339, record[4])

	return o
}

func (o Order) ToRecord() []string {
	var res []string
	cocoas := items.EncodeArray(o.Cocoas)
	hash := fnv.New64a()
	hash.Write([]byte(cocoas))
	id := hash.Sum64()

	res = append(res, fmt.Sprintf("%v", id))
	res = append(res, o.Municipality.Name)
	res = append(res, o.Municipality.County)
	res = append(res, cocoas)
	res = append(res, time.Now().Format(time.RFC3339))

	total := 0.0
	for _, c := range o.Cocoas {
		total += c.GetPrice()
	}

	res = append(res, fmt.Sprintf("%v", total))

	return res
}
