package items

import (
	"fmt"
	"strings"
)

// Size represents a size of drink
type Size int

// Order sizes
const (
	Small = iota
	Medium
	Large
	Freezing
)

// GetCocoaPrice returns the price of a cocoa based upon size
func getCocoaPrice(size Size) float64 {
	switch size {
	case Small:
		return 1.99
	case Medium:
		return 2.99
	case Large:
		return 3.99
	case Freezing:
		return 6.99
	default:
		return 0
	}
}

func (s Size) String() string {
	switch s {
	case Small:
		return "Sm"
	case Medium:
		return "Md"
	case Large:
		return "Lg"
	case Freezing:
		return "Fz"
	default:
		return ""
	}
}

// SizeFromString get a size from a string encoding
func SizeFromString(s string) Size {
	switch s {
	case "Sm":
		return Small
	case "Md":
		return Medium
	case "Lg":
		return Large
	case "Fz":
		return Freezing
	default:
		return Small
	}
}

// Flavor represents a cocoa flavor
type Flavor int

// Order flavors
const (
	Regular = iota
	Mint
	Raspberry
	SaltedCarmel
)

func (f Flavor) String() string {
	switch f {
	case Regular:
		return "Regular"
	case Mint:
		return "Mint"
	case Raspberry:
		return "Raspberry"
	case SaltedCarmel:
		return "SaltedCarmel"
	default:
		return ""
	}
}

// FlavorFromString returns a flavor from a string encoding
func FlavorFromString(f string) Flavor {
	switch f {
	case "Regular":
		return Regular
	case "Mint":
		return Mint
	case "Raspberry":
		return Raspberry
	case "SaltedCarmel":
		return SaltedCarmel
	default:
		return Regular
	}
}

// Cocoa represents a hot cocoa purchase
type Cocoa struct {
	Size   Size
	Flavor Flavor
}

// GetPrice returns the price for the cocoa order
func (c Cocoa) GetPrice() float64 {
	return getCocoaPrice(c.Size)
}

// Encode returns an encoded order
func (c Cocoa) Encode() string {
	return fmt.Sprintf("%s-%s", c.Size.String(), c.Flavor.String())
}

// DecodeCocoa returns a cocoa
func DecodeCocoa(d string) Cocoa {
	parts := strings.Split(d, "-")
	return Cocoa{Size: SizeFromString(parts[0]), Flavor: FlavorFromString(parts[1])}
}

// EncodeArray encodes an array of cocoas
func EncodeArray(cs []Cocoa) string {
	total := len(cs)
	var sb strings.Builder
	for i, elem := range cs {
		sb.WriteString(elem.Encode())
		if i < total-1 {
			sb.WriteString(":")
		}
	}
	return sb.String()
}

// DecodeArray decodes an array of cocoas
func DecodeArray(input string) []Cocoa {
	parts := strings.Split(input, ":")
	var res []Cocoa
	for _, elem := range parts {
		res = append(res, DecodeCocoa(elem))
	}
	return res
}
