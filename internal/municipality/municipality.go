package municipality

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

// Municipality representats a location in Utah.
type Municipality struct {
	MID                   int
	Name                  string `gorm:"primary_key"`
	County                string `gorm:"primary_key"`
	PopulationEstimate    int
	Area                  float64
	Elevation             int
	YearSettled           int
	MedianHouseholdIncome int
	Etymology             string `gorm:"type:text"`
	CreatedAt             time.Time
	UpdatedAt             time.Time
	DeletedAt             *time.Time
}

// FromTsv returns a Municipality or an error when parsing
func FromTsv(index int, input []string) (Municipality, error) {

	var m Municipality
	m.MID = index
	m.Name = strings.ReplaceAll(strings.Trim(input[0], " "), "*", "")
	m.County = strings.Trim(input[1], " ")
	if pop, err := strconv.Atoi(sanitizeInteger(input[2])); err == nil {
		m.PopulationEstimate = pop
	} else {
		log.Println("Error parsing population", err)
	}
	if area, err := strconv.ParseFloat(sanitizeArea(input[3]), 10); err == nil {
		m.Area = area
	} else {
		log.Println("Error parsing area", err)
	}
	if elv, err := strconv.Atoi(sanitizeElevation(input[4])); err == nil {
		m.Elevation = elv
	} else {
		log.Println("Error parsing elevation", err)
	}
	// if yr, err := strconv.Atoi(sanitizeInteger(input[5])); err == nil {
	// 	m.YearSettled = yr
	// } else {
	// 	log.Println("Error parsing settled", err)
	// }
	if inc, err := strconv.Atoi(sanitizeIncome(input[6])); err == nil {
		m.MedianHouseholdIncome = inc
	} else {
		log.Println("Error parsing income", err)
	}
	m.Etymology = strings.Trim(input[7], " ")

	return m, nil
}

func sanitizeInteger(i string) string {
	return strings.ReplaceAll(strings.ReplaceAll(i, ",", ""), " ", "")
}

func sanitizeArea(a string) string {
	parts := strings.Split(a, " sq mi ")
	return parts[0]
}

func sanitizeElevation(e string) string {
	parts := strings.Split(e, " feet ")
	return sanitizeInteger(parts[0])
}

func sanitizeIncome(i string) string {
	return strings.ReplaceAll(sanitizeInteger(i), "$", "")
}

func (m Municipality) String() string {
	return fmt.Sprintf("MID: %d - Name: %s - County: %s", m.MID, m.Name, m.County)
}
