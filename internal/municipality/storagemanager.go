package municipality

import "github.com/jinzhu/gorm"

// StorageManager allows for the persistence of municipalitis
type StorageManager struct {
	db *gorm.DB
}

// NewStorageManager creates a new storage manager and migrates the municipality
func NewStorageManager(db *gorm.DB) *StorageManager {
	sm := &StorageManager{db: db}
	sm.db.AutoMigrate(&Municipality{})
	return sm
}

// SaveAll saves an array of municipalities
func (sm *StorageManager) SaveAll(ms []Municipality) error {
	tx := sm.db.Begin()
	for _, m := range ms {
		tx.Create(&m)
	}
	tx.Commit()
	return nil
}

// ListByCounty returns municipalities for a county
func (sm *StorageManager) ListByCounty(county string) ([]Municipality, error) {
	var ms []Municipality
	err := sm.db.Where(&Municipality{County: county}).Find(&ms).Error
	return ms, err
}
