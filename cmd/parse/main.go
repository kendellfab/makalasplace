package main

import (
	"encoding/csv"
	"flag"
	"io"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/kendellfab/makalasplace/internal/municipality"
)

func main() {
	var municipalitiesFile string
	flag.StringVar(&municipalitiesFile, "m", "cities.tsv", "Set the municipalities file to read, defaults to cities.txt")
	flag.Parse()

	municipalities, err := loadMunicipalities(municipalitiesFile)
	if err != nil {
		log.Fatal("Error loading municipalities", err)
	}

	db, dbErr := gorm.Open("sqlite3", "data.db")
	if dbErr != nil {
		log.Fatal("Could not open db", dbErr)
	}
	defer db.Close()

	msm := municipality.NewStorageManager(db)
	msm.SaveAll(municipalities)

	cache, err := msm.ListByCounty("Cache")
	log.Println("Cache:", cache, err)
}

func loadMunicipalities(inputFile string) ([]municipality.Municipality, error) {
	input, err := os.Open(inputFile)
	if err != nil {
		return nil, err
	}
	defer input.Close()

	r := csv.NewReader(input)
	r.Comma = '\t'
	r.LazyQuotes = true

	var results []municipality.Municipality
	i := 1
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return results, err
		}

		m, mErr := municipality.FromTsv(i, record)
		if mErr != nil {
			return results, mErr
		} else {
			i++
			results = append(results, m)
		}
	}

	return results, nil
}
