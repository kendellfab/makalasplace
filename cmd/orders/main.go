package main

import (
	"encoding/csv"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/kendellfab/makalasplace/internal/items"
	"gitlab.com/kendellfab/makalasplace/internal/municipality"
	"gitlab.com/kendellfab/makalasplace/internal/orders"
)

func main() {

	db, dbErr := gorm.Open("sqlite3", "data.db")
	if dbErr != nil {
		log.Fatal("Could not open db", dbErr)
	}
	defer db.Close()

	msm := municipality.NewStorageManager(db)

	high, err := msm.ListByCounty("Cache")
	low, err := msm.ListByCounty("Box Elder")
	medium, err := msm.ListByCounty("Davis")

	out, err := os.Create("orders.csv")
	if err != nil {
		log.Fatal("Error opening output file", err)
	}
	defer out.Close()
	rand.Seed(time.Now().Unix())

	w := csv.NewWriter(out)
	w.Write([]string{"ID", "Town", "County", "Items", "When", "Total"})
	for i := 0; i < 50000; i++ {
		o := createOrder(high, medium, low)
		w.Write(o.ToRecord())
	}
	w.Flush()
}

func createOrder(high []municipality.Municipality, medium []municipality.Municipality, low []municipality.Municipality) orders.Order {
	o := orders.Order{}
	o.When = time.Now()

	municipalityRandom := rand.Intn(100)
	if municipalityRandom <= 65 {
		townRand := rand.Intn(len(high))
		o.Municipality = high[townRand]
	} else if municipalityRandom > 65 && municipalityRandom < 90 {
		townRand := rand.Intn(len(medium))
		o.Municipality = medium[townRand]
	} else {
		townRand := rand.Intn(len(low))
		o.Municipality = low[townRand]
	}

	howManyCocoas := rand.Intn(8) + 1
	for i := 0; i <= howManyCocoas; i++ {
		c := items.Cocoa{}
		flavorRandom := rand.Intn(4)
		c.Flavor = items.Flavor(flavorRandom)
		sizeRandom := rand.Intn(4)
		c.Size = items.Size(sizeRandom)
		o.Cocoas = append(o.Cocoas, c)
	}

	return o
}
